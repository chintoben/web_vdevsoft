﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using System.Web.Security;
using System.Net.Http;
using System.Configuration;
using System.Net.Http.Headers;
using System.Net.Http.Formatting;
using System.Diagnostics;
using System.Net;
using System.IO;
using System.Net.Mail;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

namespace VdevWebDemo.Controllers
{
    public class ProductController : Controller
    {
        SqlConnection conn = new SqlConnection(GlobalVal.Strconn);
        // GET: Product

        public void test()
        {
            SmtpClient smtpserver = new SmtpClient("mail.vdevsoft.co.th");
            var mail = new MailMessage();
            mail.From = new MailAddress("kittitap@vdevsoft.co.th");
            mail.To.Add("kittitap@vdevsoft.co.th");
            
            mail.Subject = "ทดสอบ การส่ง Email";
            mail.IsBodyHtml = true;
            string temptext = " <h4>ตัวใหญ่</h4>  <br /> <br />  <div> ตัวเล็ก </div>";
            mail.Body = temptext;

            smtpserver.Port = 587;
            smtpserver.UseDefaultCredentials = false;
            smtpserver.Credentials = new System.Net.NetworkCredential("kittitap@vdevsoft.co.th", "!Q@W#E1q2w3e");
            smtpserver.EnableSsl = true;

            ServicePointManager.ServerCertificateValidationCallback =
              delegate (
                  object s,
                  X509Certificate certificate,
                  X509Chain chain,
                  SslPolicyErrors sslPolicyErrors
              ) {
                  return true;
              };


            smtpserver.Send(mail);

        }
     

        public ActionResult Index()
        {
            return View();
        }
      
        public ActionResult ProductList()
        {
            return View();
        }
        public ActionResult ProductListIOT()
        {
            return View();
        }
        public ActionResult ProductListfarm()
        {
            return View();
        }
        public ActionResult Cart()
        {
            return View();
        }
        public ActionResult Confirmation()
        {
            return View();
        }
        public ActionResult ProductDetail()
        {
            return View();
        }

        [Route("GetProduct/{id}/{id2}")]
        public string GetProduct(string id , string id2)
        {
            string SQL;

            SQL = " SELECT * from tbProduct";
            SQL += " where 1=1 ";
            if (id != "ALL")
            {
                SQL += " and tbProduct.[ProductGroup] = '" + id + "'";
            }
            if (id2 != "ALL")
            {
                SQL += " and tbProduct.[ProductFamily] = '" + id2 + "'";
            }
            SqlDataAdapter da = new SqlDataAdapter(SQL, conn);
            DataSet ds = new DataSet();
            da.Fill(ds, "ds");
            if (ds.Tables["ds"].Rows.Count > 0)
            {
                return JsonConvert.SerializeObject(ds.Tables["ds"], Formatting.Indented);
            }
            else
            {
                return JsonConvert.SerializeObject("", Formatting.Indented);
            }
        }

        [Route("GetDelivery/{id}/{id2}")]
        public string GetDelivery(string id, string id2)
        {
            string SQL;

            SQL = " SELECT * from tbDelivery";
            SQL += " where 1=1 ";
            if (id != "ALL")
            {
                SQL += " and tbDelivery.[deliveryid] = '" + id + "'";
            }
            if (id2 != "ALL")
            {
                SQL += " and tbDelivery.[deliverybrand] = '" + id2 + "'";
            }
            SqlDataAdapter da = new SqlDataAdapter(SQL, conn);
            DataSet ds = new DataSet();
            da.Fill(ds, "ds");
            if (ds.Tables["ds"].Rows.Count > 0)
            {
                return JsonConvert.SerializeObject(ds.Tables["ds"], Formatting.Indented);
            }
            else
            {
                return JsonConvert.SerializeObject("", Formatting.Indented);
            }
        }


    




    }
}