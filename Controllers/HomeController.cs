﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VdevWebDemo.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Index2()
        {
            return View();
        }
        public ActionResult ProductIndex()
        {
            return View();
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult test()
        {
            return View();
        }
        public ActionResult Recruitment()
        {
            return View();
        }
        public ActionResult ScropeOfService()
        {
            return View();
        }
        public ActionResult MyCustomer()
        {
            return View();
        }
        public ActionResult MyProject()
        {
            return View();
        }
        public ActionResult History()
        {
            return View();
        }
        public ActionResult Vision()
        {
            return View();
        }
        public ActionResult CompanyRegister()
        {
            return View();
        }
        public ActionResult Information()
        {
            return View();
        }
        public ActionResult CSR()
        {
            return View();
        }



    }
}