﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
namespace VdevWebDemo.Controllers
{
    public class BackEndController : Controller
    {
        // GET: BackEnd
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ApproveOrder()
        {
            return View();
        }


        SqlConnection conn = new SqlConnection(GlobalVal.Strconn);

        [HttpPost]
        public string updatepaymentstatus(string paymentstatus, string orderid)
        {
            string SQL;
            SQL = " update  tbOrder set";
            SQL += " paymentstatus = '" + paymentstatus + "'";
            SQL += " where orderid = '" + orderid + "'";
            try
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                conn.Open();
                SqlCommand CMD = new SqlCommand(SQL, conn);
                CMD.ExecuteNonQuery();
                return "Data has been saved";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        [HttpPost]
        public string updatedeliverystatus(string deliverystatus, string orderid)
        {
            string SQL;
            SQL = " update  tbOrder set";
            SQL += " deliverystatus = '" + deliverystatus + "'";
            SQL += " where orderid = '" + orderid + "'";
            try
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                conn.Open();
                SqlCommand CMD = new SqlCommand(SQL, conn);
                CMD.ExecuteNonQuery();
                return "Data has been saved";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

    }
}