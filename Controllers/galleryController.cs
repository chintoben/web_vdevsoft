﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using System.Web.Security;

namespace VdevWebDemo.Controllers
{
    public class galleryController : Controller
    {
        // GET: gallery
        SqlConnection conn = new SqlConnection(GlobalVal.Strconn);
        public ActionResult Index()
        {
            return View();
        }
        [Route("getalbum/{id}")]
        public string getalbum(string id, string apikey)
        {

            string SQL;
            if (id.ToUpper() != "ALL")
            {
                SQL = " Select top " + id + " * from tbalbum order by albumid desc";
            }
            else
            {
                SQL = " Select * from tbalbum  order by albumid desc";
            }

            SqlDataAdapter da = new SqlDataAdapter(SQL, conn);
            DataSet ds = new DataSet();
            da.Fill(ds, "ds");

            if (ds.Tables["ds"].Rows.Count > 0)
            {
                return JsonConvert.SerializeObject(ds.Tables["ds"], Formatting.Indented);
            }
            else
            {
                return JsonConvert.SerializeObject("", Formatting.Indented);
            }
        }

        [Route("getgallery/{id}")]
        public string getgallery(string id, string apikey)
        {

            string SQL;
            if (id.ToUpper() != "ALL")
            {
                SQL = " Select top " + id + " * from tbgallery left join tbalbum on tbalbum.albumid = tbgallery.albumid ";
            }
            else
            {
                SQL = " Select * from tbgallery left join tbalbum on tbalbum.albumid = tbgallery.albumid ";
            }

            SqlDataAdapter da = new SqlDataAdapter(SQL, conn);
            DataSet ds = new DataSet();
            da.Fill(ds, "ds");

            if (ds.Tables["ds"].Rows.Count > 0)
            {
                return JsonConvert.SerializeObject(ds.Tables["ds"], Formatting.Indented);
            }
            else
            {
                return JsonConvert.SerializeObject("", Formatting.Indented);
            }
        }

    }
}