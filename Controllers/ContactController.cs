﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using System.Web.Security;
namespace VdevWebDemo.Controllers
{
    public class ContactController : Controller
    {
        // GET: Contact
        public ActionResult Index()
        {
            return View();
        }

        SqlConnection conn = new SqlConnection(GlobalVal.Strconn);
        [HttpPost]
        public string savecontact(string loginid, string contactname, string contactemail, string contacttitlel, string contactdescription)
        {

            string SQL;
            SQL = " insert into [tbContact] (";
            SQL += " [LoginID] ,[contactname] ,[contactemail] ,[contacttitle],[contactdescription] ,[createby] ";
            SQL += " ) Values (";
            SQL += " '" + loginid + "','" + contactname + "','" + contactemail + "','" + contacttitlel + "'";
            SQL += " ,'" + contactdescription + "',''";
            SQL += " )";

            try
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                conn.Open();
                SqlCommand CMD = new SqlCommand(SQL, conn);
                CMD.ExecuteNonQuery();
                return "Data has been saved";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }


    }
}