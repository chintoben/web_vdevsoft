﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using System.Web.Security;
using System.IO;

using System.Net;
using System.Net.Mail;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

using Newtonsoft.Json.Linq;


namespace VdevWebDemo.Controllers
{
    public class OrderController : Controller
    {
        SqlConnection conn = new SqlConnection(GlobalVal.Strconn);
        // GET: Order
        public ActionResult MyOrder()
        {
            return View();
        }
        public ActionResult HowtoOrder()
        {
            return View();
        }

        [Route("getOrder/{id}/{id2}/{id3}/{id4}")]
        public string getOrder(string id, string id2, string id3, string id4)
        {
            string SQL;
            SQL = " Select tbOrderDetail.* ,tbOrder.orderid , tbProduct.pic1 as pic1 ,tbOrder.paymentpic as paymentpic ";
            SQL += "  , productname,convert(char(10),AddCartDateTime ,103) as AddCartDateTime1 , ordertotal ";
            SQL += " , paymentstatus ,paymentamt , PaymentRemark ,deliverystatus , paymentstatus + deliverystatus as processstatus ";
            SQL += " from tbOrder ";
            SQL += " left join tbOrderDetail on tbOrderDetail.OrderID = tbOrder.orderid ";
            SQL += " left join tbProduct on tbProduct.productid = tbOrderDetail.ProductID ";
            SQL += " left join( ";
            SQL += " SELECT orderid, sum(total)as ordertotal  FROM[tbOrderdetail] group by orderid ";
            SQL += " ) as t1 on t1.orderid = [tbOrder].orderid ";


            SQL += " where 1=1 ";
      
            if (id != "ALL")
            {
                SQL += " and Loginid = '"+ id + "' ";
            }
            if (id2 != "ALL")
            {
                SQL += " and tbOrder.orderid = '" + id2 + "' ";
            }
            if (id3 != "ALL")
            {
                SQL += " and paymentstatus + deliverystatus between '" + id3 + "' and '" + id4 + "' ";
            }
            SqlDataAdapter da = new SqlDataAdapter(SQL, conn);
            DataSet ds = new DataSet();
            da.Fill(ds, "ds");

            if (ds.Tables["ds"].Rows.Count > 0)
            {
                return JsonConvert.SerializeObject(ds.Tables["ds"], Formatting.Indented);
            }
            else
            {
                return JsonConvert.SerializeObject("", Formatting.Indented);
            }

        }

        [Route("getorder_master/{id}/{id2}/{id3}/{id4}")]
        public string getOrder_master(string id, string id2, string id3, string id4)
        {
            string SQL;

            SQL = " Select sum(tbOrderDetail.total) as ordertotal, tbOrder.* ,tbOrder.paymentpic as paymentpic  ";
            SQL += " , paymentstatus + deliverystatus as processstatus ,convert(char(10),OrderDate ,103) as OrderDate1 ";
            SQL += " , case when paymentstatus = '1' then 'ชำระเงินแล้ว' when paymentstatus = '2' then 'อนุมัติแล้ว' else 'ยังไม่ได้ชำระเงิน' end as paymentstatus1 ";
            SQL += "  from tbOrder ";
            SQL += "  left join tbOrderDetail on tbOrderDetail.OrderID = tbOrder.orderid ";
            SQL += " where 1=1 ";
            if (id != "ALL")
            {
                SQL += " and Loginid = '" + id + "' ";
            }
            if (id2 != "ALL")
            {
                SQL += " and tbOrder.orderid = '" + id2 + "' ";
            }
            if (id3 != "ALL")
            {
                SQL += " and paymentstatus + deliverystatus between '" + id3 + "' and '" + id4 + "' ";
            }
            SQL += " group by tbOrder.orderid ,tbOrder.paymentpic ,tbOrder.BillEmail ";
            SQL += " , paymentstatus ,paymentamt , PaymentRemark ,deliverystatus ,tbOrder.LoginID ,tbOrder.BillName ,tbOrder.BillAddress ";
            SQL += " ,tbOrder.PostCode ,tbOrder.BillTel ,tbOrder.Total ,tbOrder.Discount ,tbOrder.TotalAmt ,tbOrder.BankName ,tbOrder.PaymentDateTime ";
            SQL += " ,tbOrder.OrderDate ,tbOrder.Deliveryname  ,tbOrder.Deliveryprice ,tbOrder.TrackingStatus ,tbOrder.StatusFlag ,tbOrder.DeliveryTrackingNumber ";
            SQL += " ,tbOrder.province ,tbOrder.ampor ,tbOrder.tambon ,tbOrder.orderremark ";
            
            SQL += " order by tbOrder.orderid desc";


            SqlDataAdapter da = new SqlDataAdapter(SQL, conn);
            DataSet ds = new DataSet();
            da.Fill(ds, "ds");

            string result = "";
            result += "[";
            for (int i = 0; i <= ds.Tables["ds"].Rows.Count - 1; i++)
            {
                if (i > 0)
                {
                    result += ",";
                }
                result += FusionSQL(ds.Tables["ds"].Rows[i]["orderid"].ToString());
            }
            result += "]";
            return result;

        }

        DataSet ds_x = new DataSet();
        DataSet ds_x1 = new DataSet();
        DataSet ds_x2 = new DataSet();
        public string FusionSQL(string orderid) // ทำ SQL 2 Statement เป็น Json 2 Dimension
        {
            SqlConnection conn = new SqlConnection(GlobalVal.Strconn);
            string SQL;
            SQL = " Select sum(tbOrderDetail.total) as ordertotal, tbOrder.* ,tbOrder.paymentpic as paymentpic  ";
            SQL += " , paymentstatus + deliverystatus as processstatus ,convert(char(10),OrderDate ,103) as OrderDate1 ";
            SQL += " , case when paymentstatus = '1' then 'ชำระเงินแล้ว' when paymentstatus = '2' then 'อนุมัติแล้ว' else 'ยังไม่ได้ชำระเงิน' end as paymentstatus1 ";
            SQL += "  from tbOrder ";
            SQL += "  left join tbOrderDetail on tbOrderDetail.OrderID = tbOrder.orderid ";
            SQL += " where 1=1 ";
            SQL += " and tbOrder.orderid = '" + orderid + "' ";
            SQL += " group by tbOrder.orderid ,tbOrder.paymentpic ,tbOrder.BillEmail";
            SQL += " , paymentstatus ,paymentamt , PaymentRemark ,deliverystatus ,tbOrder.LoginID ,tbOrder.BillName ,tbOrder.BillAddress ";
            SQL += " ,tbOrder.PostCode ,tbOrder.BillTel ,tbOrder.Total ,tbOrder.Discount ,tbOrder.TotalAmt ,tbOrder.BankName ,tbOrder.PaymentDateTime ";
            SQL += " ,tbOrder.OrderDate ,tbOrder.Deliveryname  ,tbOrder.Deliveryprice  ,tbOrder.TrackingStatus ,tbOrder.StatusFlag ,tbOrder.DeliveryTrackingNumber";
            SQL += " ,tbOrder.province ,tbOrder.ampor ,tbOrder.tambon ,tbOrder.orderremark ";
            ds_x.Clear();
            ds_x.Dispose();
            SqlDataAdapter da = new SqlDataAdapter(SQL, conn);
            da.Fill(ds_x, "A");

            SQL = " select * from [tbOrderDetail] ";
            SQL += " left join tbProduct on tbProduct.productid = tbOrderDetail.ProductID ";
            SQL += " where orderid = '" + orderid + "' ";
            da = new SqlDataAdapter(SQL, conn);
            da.Fill(ds_x, "B");

            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.Converters.Add(new CustomDataSetConverter());
            settings.Formatting = Formatting.Indented;
            string json = "";
            json += JsonConvert.SerializeObject(ds_x, settings);
            return json;
        }

        [HttpPost]
        public string SaveOrder(string loginid, string billaddress, string billtel, string billemail, string total, string PaymentStatus , string attachfile ,string deliveryprice, string deliveryname)
        {
            string newOrderID;
            string SQL;
            SQL = " insert into tbOrder (";
            SQL += " loginid ,billaddress ,billtel ,total ,PaymentStatus , Paymentpic, deliveryprice, deliveryname  ,billemail ";
            SQL += " ) Values (";
            SQL += " '" + loginid + "','" + billaddress + "','" + billtel + "'";
            SQL += " ,'" + total + "','" + PaymentStatus + "','" + attachfile + "'";
            SQL += " ,'" + deliveryprice + "','" + deliveryname + "','" + billemail + "'";
            SQL += " )";
            SQL += " select top 1 * from tbOrder order by orderid desc";
            try
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                conn.Open();
                SqlCommand CMD = new SqlCommand(SQL, conn);
                newOrderID = CMD.ExecuteScalar().ToString();
                return newOrderID ;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        [HttpPost]
        public string SaveOrderDetail(string orderid, string productid, string qty, string price, string discountrate, string discount)
        {

            string SQL;
            SQL = " insert into tbOrderDetail (";
            SQL += " orderid ,productid ,qty ,price ,discountrate , discount ,total ";
            SQL += " ) Values (";
            SQL += " '" + orderid + "','" + productid + "','" + qty + "'";
            SQL += " ,'" + price + "','" + discountrate + "','" + discount + "','" + Convert.ToInt16(qty) * Convert.ToDouble(price) + "'";
            SQL += " )";

            try
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                conn.Open();
                SqlCommand CMD = new SqlCommand(SQL, conn);
                CMD.ExecuteNonQuery();
                return "Data has been saved / " + SQL;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }



        [HttpPost]
        public string savepaymentslip(string paymentamt, string paymentremark, string orderid, HttpPostedFileBase paymentpic)
        {
            string SQL;
            SQL = " update  tbOrder set";
            SQL += " paymentpic = '"+ FileUpload(paymentpic) + "'";
            SQL += " ,paymentdatetime = getdate()";
            SQL += " ,paymentamt = '" + paymentamt + "'";
            SQL += " ,paymentremark = '" + paymentremark + "'";
            SQL += " ,paymentstatus = 1";
            SQL += " where Orderid = '"+ orderid + "'";

            try
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                conn.Open();
                SqlCommand CMD = new SqlCommand(SQL, conn);
                CMD.ExecuteNonQuery();
                return "Data has been saved";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }

        public string FileUpload(HttpPostedFileBase file)
        {
            if (file != null)
            {
                string pic = System.IO.Path.GetFileName(file.FileName);
                string path = System.IO.Path.Combine(
                Server.MapPath("~/assets/img/payment"), DateTime.Now.ToString("yyyyMMddhhmmss") + "_" + pic);
                file.SaveAs(path);
                using (MemoryStream ms = new MemoryStream())
                {
                    file.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                }
                string fname = DateTime.Now.ToString("yyyyMMddhhmmss") + "_" + pic;
                return fname;

            }
            else
            {
                return "";
            }
        }
        [HttpPost]
        public string sendmail(string sendto , string sendsubject, string senddata )
        {
            senddata = senddata.Replace(" ","");

            try
            {
                SmtpClient smtpserver = new SmtpClient("mail.vdevsoft.co.th");
                var mail = new MailMessage();
                mail.From = new MailAddress("kittitap@vdevsoft.co.th");
                mail.To.Add(sendto);
                mail.Subject = sendsubject;
                mail.IsBodyHtml = true;
                string temptext = senddata;
                mail.Body = temptext;
                smtpserver.Port = 587;
                smtpserver.UseDefaultCredentials = false;
                smtpserver.Credentials = new System.Net.NetworkCredential("kittitap@vdevsoft.co.th", "!Q@W#E1q2w3e");
                smtpserver.EnableSsl = true;
                ServicePointManager.ServerCertificateValidationCallback =
                  delegate (
                      object s,
                      X509Certificate certificate,
                      X509Chain chain,
                      SslPolicyErrors sslPolicyErrors
                  ) {
                      return true;
                  };
                smtpserver.Send(mail);
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
 

        }


    }
}



class CustomDataSetConverter : JsonConverter
{
    public override bool CanConvert(Type objectType)
    {
        return (objectType == typeof(DataSet));
    }
    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
    {
        DataSet x = (DataSet)value;
        JObject jObject = new JObject();
        DataTable a = x.Tables["A"];
        foreach (DataColumn col in a.Columns)
        {
            jObject.Add(col.Caption.ToLower(), a.Rows[0][col].ToString());
        }
        JArray jArray = new JArray();
        DataTable b = x.Tables["B"];

        foreach (DataRow row in b.Rows)
        {
            JObject jo = new JObject();
            foreach (DataColumn col in b.Columns)
            {
                jo.Add(col.Caption.ToLower(), row[col].ToString());
            }
            jArray.Add(jo);
        }
        jObject.Add("Details", jArray);
        jObject.WriteTo(writer);
    }
    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
    {
        throw new NotImplementedException();
    }
}