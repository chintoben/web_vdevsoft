﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using System.Web.Security;
namespace VdevWebDemo.Controllers
{
    public class NewsController : Controller
    {

        SqlConnection conn = new SqlConnection(GlobalVal.Strconn);
        public ActionResult Index()
        {
            return View();
        }

        [Route("getNews/{id}")]
        public string getNews(string id, string apikey)
        {

            string SQL;
            if (id != "ALL")
            {
                SQL = " Select top " + id + " * from tbnews order by newsid desc";
            }
            else
            {
                SQL = " Select * from tbnews  order by newsid desc";
            }

            SqlDataAdapter da = new SqlDataAdapter(SQL, conn);
            DataSet ds = new DataSet();
            da.Fill(ds, "ds");

            if (ds.Tables["ds"].Rows.Count > 0)
            {
                return JsonConvert.SerializeObject(ds.Tables["ds"], Formatting.Indented);
            }
            else
            {
                return JsonConvert.SerializeObject("", Formatting.Indented);
            }
        }

    }
}