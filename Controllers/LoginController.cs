﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using System.Web.Security;
using System.IO;

namespace VdevWebDemo.Controllers
{
    public class LoginController : Controller
    {
        SqlConnection conn = new SqlConnection(GlobalVal.Strconn);
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Register()
        {
            return View();
        }
        public ActionResult UserProfile()
        {
            return View();
        }
        [Route("getLogin/{id}/{id2}")]  
        public string getLogin(string id, string id2)
        {

            string SQL;
            SQL = " Select * from tbLogin";
            SQL += " where username = '" + id + "' and Password = '" + id2 + "'";
            SqlDataAdapter da = new SqlDataAdapter(SQL, conn);
            DataSet ds = new DataSet();
            da.Fill(ds, "ds");

            if (ds.Tables["ds"].Rows.Count > 0)
            {
                FormsAuthentication.SetAuthCookie(ds.Tables["ds"].Rows[0]["UserName"].ToString(), false);
                HttpCookie loginCookie = new HttpCookie("Vlogin");
                loginCookie["loginid"] = ds.Tables["ds"].Rows[0]["LoginID"].ToString();
                loginCookie["uname"] = ds.Tables["ds"].Rows[0]["UserName"].ToString();
                loginCookie["permission"] = ds.Tables["ds"].Rows[0]["UserName"].ToString();
                loginCookie["sid"] = "";
                loginCookie["picture"] = ds.Tables["ds"].Rows[0]["Pic1"].ToString();
                loginCookie.Expires = DateTime.Now.AddDays(365d);
                loginCookie["authen"] = "OK";
                Response.Cookies.Add(loginCookie);
                return JsonConvert.SerializeObject(ds.Tables["ds"], Formatting.Indented);
            }
            else
            {
                return JsonConvert.SerializeObject("", Formatting.Indented);
            }
        }
        [Route("getLoginbyid/{id}")]
        public string getLoginbyid(string id)
        {

            string SQL;
            SQL = " Select * from tbLogin";
            SQL += " where loginid = '" + id + "'";
            SqlDataAdapter da = new SqlDataAdapter(SQL, conn);
            DataSet ds = new DataSet();
            da.Fill(ds, "ds");

            if (ds.Tables["ds"].Rows.Count > 0)
            {
             
                return JsonConvert.SerializeObject(ds.Tables["ds"], Formatting.Indented);
            }
            else
            {
                return JsonConvert.SerializeObject("", Formatting.Indented);
            }
        }


        public ActionResult Logout()
        {
            HttpCookie loginCookie = new HttpCookie("Vlogin");
            loginCookie.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(loginCookie);
            FormsAuthentication.SignOut();

            Response.Cookies["Vlogin"].Expires = DateTime.Now.AddDays(-1);

            return RedirectToAction("Register");
        }




        [HttpPost]
        public string Register(string email , string username, string password)
        {

            string SQL;
            SQL = " insert into tblogin (";
            SQL += " eMail ,UserName ";
            SQL += " ,Password";
            SQL += " ) Values (";
            SQL += " '" + email + "','" + username + "','" + password + "'";
            SQL += " )";

            try
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                conn.Open();
                SqlCommand CMD = new SqlCommand(SQL, conn);
                CMD.ExecuteNonQuery();
                return "Data has been saved";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }


        [Route("CheckDuplicate/{id}/{id2}")]
        public string CheckDuplicate(string id, string id2)
        {

            string sqlQuery;
            sqlQuery = "  SELECT *  ";
            sqlQuery += " FROM [tbLogin]  ";
            sqlQuery += " where username = '" + id + "' or eMail = '" + id2 + "' ";

            SqlDataAdapter da = new SqlDataAdapter(sqlQuery, conn);
            DataSet ds = new DataSet();
            da.Fill(ds, "ds");

            if (ds.Tables["ds"].Rows.Count > 0)
            {
                return "DUP";
            }
            else
            {
                return "OK";
            }
        }


        [HttpPost]
        public string UpdateProfile(string fname, string lname, string username, string password, string address1, string address2
            , string postcode, string tel, string email, string tambon, string ampor, string province, string Loginid, HttpPostedFileBase pic1)
        {
            string SQL;
            SQL = " update  tbLogin set";
            SQL += " fname = '" + fname + "'";
            SQL += " ,lname = '" + lname + "'";
            SQL += " ,username = '" + username + "'";
            SQL += " ,password = '" + password + "'";
            SQL += " ,address1 = '" + address1 + "'";
            SQL += " ,address2 = '" + address2 + "'";
            SQL += " ,postcode = '" + postcode + "'";
            SQL += " ,tambon = '" + tambon + "'";
            SQL += " ,ampor = '" + ampor + "'";
            SQL += " ,province = '" + province + "'";
            SQL += " ,tel = '" + tel + "'";
            SQL += " ,email = '" + email + "'";
            if (pic1 != null)
            {
                SQL += " ,pic1 = '" + FileUpload(pic1) + "'";
            }
          
            SQL += " where Loginid = '" + Loginid + "'";
       
            try
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
                conn.Open();
                SqlCommand CMD = new SqlCommand(SQL, conn);
                CMD.ExecuteNonQuery();
                return "Data has been saved";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                conn.Close();
            }
        }
        public string FileUpload(HttpPostedFileBase file)
        {
            if (file != null)
            {
                string pic = System.IO.Path.GetFileName(file.FileName);
                string path = System.IO.Path.Combine(
                Server.MapPath("~/assets/img/Avatar"), DateTime.Now.ToString("yyyyMMddhhmmss") + "_" + pic);
                file.SaveAs(path);
                using (MemoryStream ms = new MemoryStream())
                {
                    file.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                }
                string fname = DateTime.Now.ToString("yyyyMMddhhmmss") + "_" + pic;
                return fname;

            }
            else
            {
                return "";
            }
        }

    }
}