﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using CrystalDecisions.CrystalReports.Engine;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;


namespace VdevWebDemo.Controllers
{
    public class PrintController : Controller
    {
        SqlConnection conn = new SqlConnection(GlobalVal.Strconn);

        [HttpGet]
        [Route("PrintCover/{id}")]
        public ActionResult PrintCover(string id)
        {

            string SQL = "";
            //SQL = "  SET LANGUAGE Thai ";
            //SQL += " select title, fname, lname ";
            //SQL += " , address1, moo, tambon, ampor, province ";
            //SQL += " , 'ที่อยู่ ' + address1 + ' หมู่ ' + moo + ' ตำบล ' + replace(tambon, 'ต. ', '') + ' อำเภอ ' + replace(ampor, 'อ. ', '') + ' จังหวัด ' + replace(province, 'จ. ', '') as address2 ";
            //SQL += "  ,plantname ,CertificateCreateDate as createdate , CertificateExpireDate as [expiredate] ";
            //SQL += "  ,trim(STR(Day(CertificateCreateDate))) + ' เดือน ' + trim(DATENAME(MONTH, CertificateCreateDate)) + ' ' + trim(STR(YEAR(CertificateCreateDate) + 543)) AS createdate2 ";
            //SQL += " , trim(STR(Day(CertificateExpireDate))) +' เดือน ' + trim(DATENAME(MONTH, CertificateExpireDate)) + ' ' + trim(STR(YEAR(CertificateExpireDate) + 543)) AS [expiredate2] ";
            //SQL += "  from [Tb_Certificate] ";
            //SQL += "  left join[Tb_login] on[Tb_login].loginid = [Tb_Certificate].loginid ";
            //SQL += "   left join[Tb_plant] on[Tb_plant].plantid = [Tb_Certificate].plantid ";
            //SQL += "   where Tb_Certificate.CertificateID = '" + id + "'";
            try
            {
                //DataSet ds = new DataSet1();
                //SqlDataAdapter da = new SqlDataAdapter(SQL, conn);
                //da.Fill(ds, "ds");
                ReportDocument rd = new ReportDocument();
                rd.Load(Path.Combine(Server.MapPath("~/CrystalReports"), "cr_cover.rpt"));
                //rd.SetDataSource(ds.Tables["ds"]);
                Response.Buffer = false;
                Response.ClearContent();
                Response.ClearHeaders();
                Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                return File(stream, "application/pdf", "GAP - " + DateTime.Now.ToString("yyyyMMddHHmm") + ".pdf");
            }
            catch (Exception ex)
            {
                return View(ex.Message);
            }




        }
    }
}