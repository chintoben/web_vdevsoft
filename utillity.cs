﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VdevWebDemo
{
    public static class utillity
    {

        public static string IsActive(this HtmlHelper html, string control, string action)
        {
            var routedata = html.ViewContext.RouteData;
            var routeAction = (string)routedata.Values["action"];
            var routeControl = (string)routedata.Values["controller"];
            var returnActive = control == routeControl && action == routeAction;
            return returnActive ? "active" : "";
        }


    }
}